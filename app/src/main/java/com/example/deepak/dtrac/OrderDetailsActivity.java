package com.example.deepak.dtrac;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IntegerRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.deepak.dtrac.adapters.OrdersListAdapter;

import java.util.ArrayList;

/**
 * Created by deepak on 02/03/17.
 */

public class OrderDetailsActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private OrdersListAdapter recyclerViewAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        recyclerView = (RecyclerView) findViewById(R.id.orders_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        ArrayList<Orders> ordersDataset = new ArrayList<Orders>();
        Orders order1 = new Orders(1,"2-Pizza","Mumbai","Vikas Marg, Laxmi Nagar Commercial Complex, Swasthya Vihar, New Delhi, Delhi");
        Orders order2 = new Orders(2,"1-Pizza","Delhi","East Guru Angad Nagar Swasthya Vihar New Delhi, Delhi 110092");
        Orders order3 = new Orders(3,"3-Pizza","Kolkata","East Guru Angad Nagar Swasthya Vihar New Delhi, Delhi 110092");
        Orders order4 = new Orders(4,"4-Pizza","Punjab","East Guru Angad Nagar Swasthya Vihar New Delhi, Delhi 110092");

        ordersDataset.add(order1);
        ordersDataset.add(order2);
        ordersDataset.add(order3);
        ordersDataset.add(order4);


        recyclerViewAdapter = new OrdersListAdapter(ordersDataset);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
