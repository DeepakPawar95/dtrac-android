package com.example.deepak.dtrac.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.deepak.dtrac.MapsActivity;
import com.example.deepak.dtrac.Orders;
import com.example.deepak.dtrac.R;

import java.util.ArrayList;

/**
 * Created by deepak on 02/03/17.
 */

public class OrdersListAdapter extends RecyclerView.Adapter<OrdersListAdapter.ViewHolder> {

    private ArrayList<Orders> ordersDataset;

    public OrdersListAdapter(ArrayList<Orders> ordersDataset){ this.ordersDataset = ordersDataset; }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView orderId;
        private TextView orderInfo;
        private TextView address;
        private TextView restaurant;
        private Button startOrderBtn;

        private Context context;

        ViewHolder(View view){
            super(view);

            orderId = (TextView) view.findViewById(R.id.orderId);
            orderInfo = (TextView) view.findViewById(R.id.orderInfo);
            address = (TextView) view.findViewById(R.id.address);
            restaurant = (TextView) view.findViewById(R.id.restaurant);
            startOrderBtn = (Button) view.findViewById(R.id.startOrderBtn);

            context = view.getContext();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.each_order_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final Orders order = ordersDataset.get(position);

        holder.orderId.setText(order.getId().toString());
        holder.orderInfo.setText(order.getInfo());
        holder.address.setText(order.getAddress());
        holder.restaurant.setText(order.getRestaurant());

        holder.startOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.context, MapsActivity.class);
                intent.putExtra("Order", order);
                holder.context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return ordersDataset.size();
    }
}
