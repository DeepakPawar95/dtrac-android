package com.example.deepak.dtrac;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by deepak on 01/03/17.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText emailField;
    private EditText pwdField;
    private Button logInBtn;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emailField = (EditText) findViewById(R.id.agentEmail);
        pwdField = (EditText) findViewById(R.id.agentPwd);

        logInBtn = (Button) findViewById(R.id.logInBtn);
        logInBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String email = emailField.getText().toString();
        String pwd = pwdField.getText().toString();

        if(email.equals("")){
            Toast.makeText(getApplicationContext(), "Please enter your email", Toast.LENGTH_SHORT).show();
            return;
        }
        if(pwd.equals("")){
            Toast.makeText(getApplicationContext(), "Please enter a password", Toast.LENGTH_SHORT).show();
            return;
        }

        logIn(email, pwd);
    }

    private void logIn(final String email, final String pwd){

        if(email.equals("ziksa@gmail.com") && pwd.equals("ziksa@123")){
            Toast.makeText(getApplicationContext(), "Login successful", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }
        else{
            Toast.makeText(getApplicationContext(), "Email or Password invalid", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, SplashActivity.class);
        startActivity(intent);
    }
}
