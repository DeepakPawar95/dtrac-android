package com.example.deepak.dtrac;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by deepak on 01/03/17.
 */

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText nameField;
    private EditText emailField;
    private EditText pwdField;
    private EditText confirmPwdField;
    private Button signUpBtn;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        nameField = (EditText) findViewById(R.id.agentName);
        emailField = (EditText) findViewById(R.id.agentEmail);
        pwdField = (EditText) findViewById(R.id.agentPwd);
        confirmPwdField = (EditText) findViewById(R.id.confirmPwd);

        signUpBtn = (Button) findViewById(R.id.signUpBtn);
        signUpBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        String name = nameField.getText().toString();
        String email = emailField.getText().toString();
        String pwd = pwdField.getText().toString();
        String confirmPwd = confirmPwdField.getText().toString();

        if(name.equals("")){
            Toast.makeText(getApplicationContext(), "Please enter a name", Toast.LENGTH_SHORT).show();
            return;
        }
        if(email.equals("")){
            Toast.makeText(getApplicationContext(), "Please enter your email", Toast.LENGTH_SHORT).show();
            return;
        }
        if(pwd.equals("")){
            Toast.makeText(getApplicationContext(), "Please enter a password", Toast.LENGTH_SHORT).show();
            return;
        }
        if(confirmPwd.equals("")){
            Toast.makeText(getApplicationContext(), "Please enter a confirm password", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!pwd.equals(confirmPwd)){
            Toast.makeText(getApplicationContext(), "Password and confirm password does not match", Toast.LENGTH_SHORT).show();
            return;
        }

        signUp(name, email, pwd);
    }

    private void signUp(final String name, final String email, final String pwd){

        Toast.makeText(getApplicationContext(), "Registration successful", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    public void onBackPressed() {
        Intent intent = new Intent(this, SplashActivity.class);
        startActivity(intent);
    }
}
