package com.example.deepak.dtrac;

import java.io.Serializable;

/**
 * Created by deepak on 02/03/17.
 */

public class Orders implements Serializable {
    private Integer id;
    private String info;
    private String address;
    private String restaurant;

    public Orders(Integer id, String info, String address, String restaurant) {
        this.id = id;
        this.info = info;
        this.address = address;
        this.restaurant = restaurant;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }
}
